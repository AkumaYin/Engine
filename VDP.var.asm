; ===========================================================================
; ---------------------------------------------------------------------------
; VDP.var.asm - RAM variables for VDP library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------

Sprite_Buffer		rs.b	$280				; VRAM sprite buffer
CRAM_Buffer		rs.b	$80				; CRAM buffer
HScroll_Buffer		rs.b	$380				; VRAM H-Scroll buffer
VScroll_Buffer		rs.b	$80				; VSRAM (V-scroll) buffer

HScroll_FG		equ	HScroll_Buffer
HScroll_BG		equ	HScroll_Buffer+$02

VScroll_FG		equ	VScroll_Buffer
VScroll_BG		equ	VScroll_Buffer+$02

DMA_Queue		rs.b	($10+$02)*DMA_QUEUE_SLOTS	; DMA queue entry slots
DMA_Queue_Stop		rs.b	$02				; stop token for DMA queue