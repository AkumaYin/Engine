; ===========================================================================
; ---------------------------------------------------------------------------
; VDP.asm - VDP-related library
; 
; Author: Vincent Clarke (AkumaYin)
; Special thanks:
; 	flamewing - DMA queue method, adapted loosely here
; ---------------------------------------------------------------------------

; ===========================================================================
; ---------------------------------------------------------------------------
; Macro to immediately DMA transfer data
; ---------------------------------------------------------------------------
; PARAMETERS:
; 	size - size of data to transfer
; 	src - location of data to transfer
; 	dest - destination of transfer (VRAM write command)
; ---------------------------------------------------------------------------

DMA	macro	size, src, dest
		move.l	#(((((size/$02)<<$08)&$FF0000)+((size/$02)&$FF))+$94009300),(VDP_Ctrl_Port).l
		move.l	#((((((src&$FFFFFF)/$02)<<$08)&$FF0000)+(((src&$FFFFFF)/$02)&$FF))+$96009500),(VDP_Ctrl_Port).l
		move.l	#(((((src&$FFFFFF)/$02)&$7F0000)+$97000000)+((dest>>$10)&$FFFF)),(VDP_Ctrl_Port).l
		move.w	#((dest&$FF7F)|$80),-(sp)
		move.w	(sp)+,(VDP_Ctrl_Port).l
	endm

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to set up the DMA queue
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

SetupDMAQueue:
		lea	(DMA_Queue).w,a0			; load start of DMA queue entries
		moveq	#$00,d0					; clear d0
		move.l	#$8F009700,d1				; first long-word: auto-increment, high byte of source
		move.l	#$96009500,d2				; second long-word: middle and low bytes of source
		move.l	#$94009300,d3				; third long-word: upper and lower bytes of size
		moveq	#DMA_QUEUE_SLOTS-1,d7			; set repeat times (number of available DMA queue entries)

.Loop:
		move.w	d0,(a0)+				; set up queue slot
		move.l	d1,(a0)+				; ''
		move.l	d2,(a0)+				; ''
		move.l	d3,(a0)+				; ''
		move.l	d0,(a0)+				; ''
		dbf	d7,.Loop				; ''

		move.w	#$FFFF,(a0)				; place stop token at the end of queue
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to add an entry to the DMA queue
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.w - Size
; 	d1.l - Source
; 	d2.w - Destination (VRAM address)
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

AddDMAQueueEntry:
		moveq	#$02,d3					; set auto-increment value

; ---------------------------------------------------------------------------
; INPUTS:
; 	d3.b - Auto-increment value
; ---------------------------------------------------------------------------

AddDMAQueueEntry_Increment:
		lsr.w	#1,d0					; divide size by two
		andi.l	#$00FFFFFF,d1				; ensure top byte of source is cleared
		lsr.l	#1,d1					; divide source by two

	; Convert VRAM address to a DMA write command

		swap	d2					; swap VRAM address to high word
		clr.w	d2					; clear low word
		swap	d2					; restore to low word
		add.l	d2,d2					; bit-shift long left by two
		add.l	d2,d2					; ''
		lsr.w	#2,d2					; bit-shift low word right by two (high word contains overflow bits from shift)
		ori.w	#$4000,d2				; append base of $4000 from VDP write command to low word
		swap	d2					; swap (order command properly)
		ori.b	#$80,d2					; append DMA flag

	; Start checking for a free slot

		lea	(DMA_Queue).w,a0			; load start of DMA queue entries
		moveq	#$00,d4					; clear d4 (start value for search)

.Search:
		bmi.s	.Return					; if we've hit the stop token, return (no free entries)
		add.w	d4,a0					; advance to next entry
		move.w	(a0),d4					; is this slot free?
		bne.s	.Search					; if not, keep searching

	; Setup entry

		movep.w	d0,DMA_Entry_Size+$01(a0)		; set size
		movep.l	d1,DMA_Entry_Source-$02+$01(a0)		; set source
		move.l	d2,DMA_Entry_Command(a0)		; set destination write-command
		move.b	d3,DMA_Entry_Increment+$01(a0)		; set auto-increment (overwrites empty top byte placed by source)
		move.w	#DMA_Entry_End,(a0)			; set this slot as in use (last step in case a VBI occurs while setting up the entry)

.Return:
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to execute the DMA queue
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

RunDMAQueue:
		lea	(DMA_Queue).w,a0			; load start of DMA queue
		lea	(VDP_Ctrl_Port).l,a6			; load VDP control port
		moveq	#$00,d0					; clear d0

.Loop:
		tst.w	(a0)					; is this slot in use?
		beq.s	.Return					; if not, return (end of active list)
		bmi.s	.Return					; if we've hit the stop token, return (entries topped out)

		move.w	d0,(a0)+				; reset slot status

		move.l	(a0)+,(a6)				; write DMA size
		move.l	(a0)+,(a6)				; write auto-increment and high byte of source
		move.l	(a0)+,(a6)				; write mid and low bytes of source
		move.l	(a0)+,(a6)				; write destination command

		bra.s	.Loop					; restart loop

.Return:
		rts						; return