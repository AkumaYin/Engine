; ===========================================================================
; ---------------------------------------------------------------------------
; Ctrl.asm - I/O library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------

; ===========================================================================
; ---------------------------------------------------------------------------
; Collect peripheral input for this frame
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------
; NOTES:
; 	currently this only collects input for both 3-button pads
; ---------------------------------------------------------------------------

ReadInput:
		lea	(Control_Data).w,a0			; load RAM space for control states
		lea	(IO_A_Data_Port).l,a1			; load joypad A data port
		moveq	#%00000000,d0				; prepare TH = 0
		moveq	#%01000000,d1				; prepare TH = 1

	; --- reading pad A ---

		move.b	d0,(a1)					; set TH = 0
		moveq	#%00110000,d2				; 
		moveq	#%00111111,d3				; 
		and.b	(a1),d2					; 
		move.b	d1,(a1)					; set TH = 1
		add.b	d2,d2					; shift button bits to the left
		add.b	d2,d2					; ''
		and.b	(a1),d3					; 
		or.b	d3,d2					; 
		not.b	d2					; 
		move.b	(a0),d3					; 
		eor.b	d2,d3					; 
		move.b	d2,(a0)+				; save all held buttons
		and.b	d2,d3					; keep only newly pressed buttons
		move.b	d3,(a0)+				; save all pressed buttons for this frame

	; --- reading pad B ---

		addq.w	#$02,a1					; advance to pad B data port
		move.b	d0,(a1)					; set TH = 0
		moveq	#%00110000,d2				; 
		moveq	#%00111111,d3				; 
		and.b	(a1),d2					; 
		move.b	d1,(a1)					; set TH = 1
		add.b	d2,d2					; shift button bits to the left
		add.b	d2,d2					; ''
		and.b	(a1),d3					; 
		or.b	d3,d2					; 
		not.b	d2					; 
		move.b	(a0),d3					; 
		eor.b	d2,d3					; 
		move.b	d2,(a0)+				; save all held buttons
		and.b	d2,d3					; keep only newly pressed buttons
		move.b	d3,(a0)+				; save all pressed buttons for this frame
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Store the dpad's directional axes signs
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------
; NOTES: give this a better description later
; ---------------------------------------------------------------------------

GetPadAxes:
		lea	(Pad_A_Held).w,a0
		lea	(P1_X_Dir_Held).w,a1
		moveq	#%00001100,d0				; %0000LR00
		moveq	#0,d1
		and.b	(a0),d0
		beq.s	.SaveX
		moveq	#1,d1
		lsl.b	#4,d0
		ext.w	d0
		bmi.s	.SaveX
		neg.w	d1

.SaveX:
		move.w	d1,(a1)+

		moveq	#%00000011,d0
		moveq	#0,d1
		and.b	(a0),d0
		beq.s	.SaveY
		moveq	#1,d1
		lsl.b	#6,d0
		ext.w	d0
		bmi.s	.SaveY
		neg.w	d1

.SaveY:
		move.w	d1,(a1)+
		rts