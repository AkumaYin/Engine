; ===========================================================================
; ---------------------------------------------------------------------------
; Obj.equ.asm - Equate/structure constants for object library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------
; Build switches
; ---------------------------------------------------------------------------

DEBUG_OBJECTS	=	1

; ---------------------------------------------------------------------------
; Equates
; ---------------------------------------------------------------------------

MAX_OBJ_NODES		equ	128			; number of object property nodes
MAX_OBJ_LISTS		equ	16			; number of object processing lists
SPR_LAYERS		equ	16			; number of sprite display layers

; ---------------------------------------------------------------------------
; Object property node structure
; ---------------------------------------------------------------------------

	rsreset

; next/prev link pointers

Obj.Next		rs.b	$02			; pointer to next object (token if it's the last in a list)
Obj.Prev		rs.b	$02			; pointer to previous object (empty if this is a free node)

Obj.First_Child		rs.b	$02			; lol
Obj.Last_Child		rs.b	$02			; lol

; next/prev link pointers for sprite display

Obj.Next_Spr		rs.b	$02			; next object in display list
Obj.Prev_Spr		rs.b	$02			; previous object in display list

; jump trampoline for routine execution

Obj.Jmp			rs.b	$02			; slot for "jmp" machine code
Obj.Jmp_Ptr		rs.b	$04			; pointer to code for this object to run (left as a pointer to the "next object" function if the object does nothing on its own)

; Position and velocity

Obj.X_Pos		rs.b	$02			; high word of fixed-point X position (pixels)
Obj.X_Sub_Pos		rs.b	$01			; low byte of fixed-point X position (subpixels)
Obj.Touch_Width		rs.b	$01			; touch width (X radius)
Obj.Y_Pos		rs.b	$02			; high word of fixed-point Y position (pixels)
Obj.Y_Sub_Pos		rs.b	$01			; low byte of fixed-point Y position (subpixels)
Obj.Touch_Height	rs.b	$01			; touch height (Y radius)
Obj.X_Vel		rs.b	$02			; X velocity (fixed-point, $8.8)
Obj.Y_Vel		rs.b	$02			; Y velocity (fixed-point, $8.8)

; VDP/sprite-related

Obj.Spr_Layer		rs.b	$02			; sprite display layer ID (multiples of 4)
Obj.Maps		rs.b	$04			; pointer to sprite mappings data
Obj.Frame		rs.b	$02			; frame ID (multiples of 2)
Obj.Render_Flags	rs.b	$01			; VDP render flags
Obj.Hide		rs.b	$01			; hide flag (object sprite won't be rendered the current frame if set)
Obj.Tile		rs.b	$02			; VDP tile index (VRAM location / $20)

Obj.Anim_ID		rs.b	$02			; lol
Obj.Anim_Script		rs.b	$04			; lol
Obj.Anim_Frame		rs.b	$01			; lol
Obj.Anim_Sub_Frame	rs.b	$01			; lol
Obj.Anim_Speed		rs.b	$02			; lol

; Misc. game variables (unsorted)

Obj.Angle		rs.b	$01
			rs.b	$01
Obj.Parent		rs.b	$02

; Dynamic variables (local structures for objects can be built off of this)

Obj.Dyn_Vars		rs.b	$20			; 32 bytes for objects to use for local variables

		;	rs.b	$10-(__rs&$0F)		; align to nearest 16 bytes (for debugging)

Obj.End			equ	__rs			; end of object structure (used as the size of a node)