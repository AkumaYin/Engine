; ===========================================================================
; ---------------------------------------------------------------------------
; Ctrl.equ.asm - Equate/structure constants for I/O library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------

IO_A_Data_Port	equ	$A10003
IO_B_Data_Port	equ	$A10005
IO_C_Data_Port	equ	$A10007

IO_A_Ctrl_Port	equ	$A10009
IO_B_Ctrl_Port	equ	$A1000B
IO_C_Ctrl_Port	equ	$A1000D