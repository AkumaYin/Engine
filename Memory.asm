; ===========================================================================
; ---------------------------------------------------------------------------
; Memory.asm - Memory library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------
; WARNING: This library is targeted towards performance over space. The
; unrolled loops here generate fairly large code blobs; using this isn't
; recommended if you have space constraints to work around.
; ---------------------------------------------------------------------------

; ===========================================================================
; ---------------------------------------------------------------------------
; Functions to copy large sections of ROM or RAM to a location in RAM
; ---------------------------------------------------------------------------
; INPUTS:
; 	a0.l - Source
; 	a1.l - Destination
; 
; RETURNS:
; 	none
; 
; NOTES:
; 	Destroyes all data registers.
; ---------------------------------------------------------------------------

CopyMemory_8000:
	rept	($8000/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_4000:
	rept	($4000/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_2000:
	rept	($2000/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_1000:
	rept	($1000/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_800:
	rept	($800/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_400:
	rept	($400/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_200:
	rept	($200/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_100:
	rept	($100/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_80:
	rept	($80/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_40:
	rept	($40/2)/$20
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
	endr

CopyMemory_20:
		movem.l	(a0)+,d0-d7
		movem.l	d0-d7,(a1)
		lea	$20(a1),a1
		rts

; ===========================================================================
; ---------------------------------------------------------------------------
; Functions to fill a section of RAM with a specified value
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.l/w/b - Value for fill
; 	d1.w - Size of fill
; 	a0.l - Address to begin fill
; 
; RETURNS:
; 	a0.l - Address at end of fill
; 
; DESTROYS:
; 	a1.l
; ---------------------------------------------------------------------------

	; Unrolled loop for filling with a byte value
	rept	$10000-1
		move.b	d0,(a0)+				; write byte value
	endr
		rts						; return

FillRAM_Byte:
		swap	d1					; swap words of size
		clr.w	d1					; clear upper word
		swap	d1					; restore word order
		add.l	d1,d1					; double size (each move instruction is two bytes)
		lea	FillRAM_Byte-$02(pc),a1			; load end of loop
		suba.l	d1,a1					; subtract size
		jmp	(a1)					; jump to proper section of loop

	; Unrolled loop for filling with a word value
	rept	($10000/2)-1
		move.w	d0,(a0)+				; write word value
	endr
		rts						; return

FillRAM_Word:
		andi.l	#$0000FFFE,d1				; round size to two bytes
		add.l	d1,d1					; double size
		lea	FillRAM_Word-$02(pc),a1			; load end of loop
		suba.l	d1,a1					; subtract size
		jmp	(a1)					; jump to proper section of loop

	; Unrolled loop for filling with a long value
	rept	($10000/4)-1
		move.l	d0,(a0)+				; write long value
	endr
		rts						; return

FillRAM_Long:
		andi.l	#$0000FFFC,d1				; round size to four bytes
		add.l	d1,d1					; double size
		lea	FillRAM_Long-$02(pc),a1			; load end of loop
		suba.l	d1,a1					; subtract size
		jmp	(a1)					; jump to proper section of loop

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to clear a section of RAM
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.w - Clear size (can be odd or even)
; 	a0.l - Address to begin clearing
; 
; RETURNS:
; 	none
; 
; DESTROYS:
; 	d0-d7/a1
; ---------------------------------------------------------------------------

	rept	($10000/$20)-1
		movem.l	d0-d7,-(a0)				; clear $20 bytes decrementally
	endr
		rts						; return

ClearRAM:
		swap	d0					; swap words of size
		clr.w	d0					; clear upper word
		swap	d0					; restore
		adda.l	d0,a0					; add size to address
		moveq	#$1F,d1					; check last 31 bits of size
		and.b	d0,d1					; ''
		beq.s	.NoExtraBytes				; if none are set, branch
		moveq	#$00,d2					; clear d2
		add.w	d1,d1					; double size of extra bytes ()
		neg.w	d1					; invert (we're jumping backwards from the end lol)
		jmp	.ClearExtra(pc,d1.w)			; jump properly to clear loose bytes (round down to $20)

	rept	$20
		move.b	d2,-(a0)				; clear a byte and decrement
	endr

.ClearExtra:
.NoExtraBytes:
		andi.w	#$FFE0,d0				; limit size
		lea	ClearRAM-$02(pc),a1			; load end of unrolled loop
		lsr.w	#3,d0					; divide by 8 (each movem instruction to clear $20 bytes is four bytes)
		suba.l	d0,a1					; get proper position in loop
		moveq	#$00,d0					; clear data registers ($20 bytes)
		move.l	d0,d1					; ''
		move.l	d1,d2					; ''
		move.l	d2,d3					; ''
		move.l	d3,d4					; ''
		move.l	d4,d5					; ''
		move.l	d5,d6					; ''
		move.l	d6,d7					; ''
		jmp	(a1)					; jump (begin clearing)