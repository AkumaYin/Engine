; ===========================================================================
; ---------------------------------------------------------------------------
; Ctrl.var.asm - Equate/structure constants for I/O library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------

Control_Data		rs.b	0
Pad_A_Held		rs.b	$01				; pad A's held buttons for this frame
Pad_A_Pressed		rs.b	$01				; pad A's pressed buttons for this frame
Pad_B_Held		rs.b	$01				; pad B's held buttons for this frame
Pad_B_Pressed		rs.b	$01				; pad B's pressed buttons for this frame

P1_X_Dir_Held		rs.b	$02
P1_Y_Dir_Held		rs.b	$02

P1_X_Dir_Pressed	rs.b	$02
P1_Y_Dir_Pressed	rs.b	$02