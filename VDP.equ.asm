; ===========================================================================
; ---------------------------------------------------------------------------
; VDP.equ.asm - Equates/structure constants for VDP library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------
; Equates
; ---------------------------------------------------------------------------

VDP_Data_Port		equ	$C00000
VDP_Ctrl_Port		equ	$C00004

DMA_QUEUE_SLOTS		equ	32

; ---------------------------------------------------------------------------
; Structure - DMA queue entry
; ---------------------------------------------------------------------------

	rsreset

DMA_Entry_Next		rs.b	$02
DMA_Entry_Increment	rs.b	$02
DMA_Entry_Source	rs.b	$02
			rs.b	$02
			rs.b	$02
DMA_Entry_Size		rs.b	$02
			rs.b	$02
DMA_Entry_Command	rs.b	$04
DMA_Entry_End		equ	__rs