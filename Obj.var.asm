; ===========================================================================
; ---------------------------------------------------------------------------
; Obj.var.asm - RAM variables for object library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------

Next_Free_Obj		rs.b	$02			; list head for free object nodes

; List heads for active object lists

Obj_Lists		rs.b	$04*MAX_OBJ_LISTS
Obj_Nodes		rs.b	Obj.End*MAX_OBJ_NODES	; space for object nodes = node structure size * max number of nodes specified
Obj_Nodes_End		rs.b	0

	if (DEBUG_OBJECTS)
Active_Objects		rs.b	$02	; hack
	endif

Sprite_Layer_Lists	rs.b	$04*SPR_LAYERS		; list heads for display lists (linked into objects set to be rendered)