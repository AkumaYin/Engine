; ===========================================================================
; ---------------------------------------------------------------------------
; Obj.asm - Object library
; 
; Author: Vincent Clarke (AkumaYin)
; ---------------------------------------------------------------------------

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to set up all object nodes and list heads
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

SetupObjNodes:
		lea	(Obj_Lists).w,a0			; load start of object list heads

	; If we're using more than one list for active objects, set up the tokens

	if MAX_OBJ_LISTS > 0
		moveq	#$00,d0					; clear d0
		moveq	#$04,d1					; set size of list heads (to increment token and address)
		rept MAX_OBJ_LISTS-1
			add.w	d1,d0				; increment "next list" token
			move.w	d0,(a0)				; save token in next object slot
			move.w	a0,$02(a0)			; link list head into itself as its last object
			add.w	d1,a0				; increment address to next list
		endr
	endif
	; ---

		moveq	#$00,d0					; clear d0
		move.w	d0,(a0)					; clear next object pointer in last list
		move.w	a0,$02(a0)				; link list head into itself as its last object

		lea	(Obj_Nodes).w,a0			; load start of object nodes
		move.w	a0,(Next_Free_Obj).w			; set in list head for free nodes
		move.w	#$4EF9,d1				; prepare "jmp" machine code
		move.l	#NextObject,d2				; prepare pointer to "next object" function
		move.w	#(MAX_OBJ_NODES-1)-1,d7			; set number of nodes to set up (-1, the last one will carry a null value instead of linking to the next)

.Loop:
		lea	Obj.End(a0),a1				; load start of next node
		move.w	a1,(a0)+				; set pointer to next object
		bsr.s	.ClearNode				; clear the rest of the node
		dbf	d7,.Loop				; loop for rest of nodes up until last one

		move.w	d0,(a0)+				; clear next pointer in the last slot
		bsr.s	.ClearNode				; clear the rest of the last node
		rts						; return

.ClearNode:
	rept (Obj.End-$02)/$04
		move.l	d0,(a0)+				; clear four bytes
	endr
	if (Obj.End-$02)&$02>0
		move.w	d0,(a0)+				; clear last two bytes
	endif

		move.w	d1,Obj.Jmp-Obj.End(a0)			; set jmp
		move.l	d2,Obj.Jmp_Ptr-Obj.End(a0)		; set jmp pointer
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to add an object to the end of an active list
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.w - ID of list to use
; 
; RETURNS:
; 	a0.l - Object node 
; 	conditions:
; 		Z clear (bne) - object node found
; 		Z set (beq) - no object node available
; ---------------------------------------------------------------------------

AddObject:
		move.w	(Next_Free_Obj).w,d1		; 12c	; get first available object node
		beq.s	.Return				; 10/8c	; 

	if (DEBUG_OBJECTS)
		addq.w	#$01,(Active_Objects).w	; hack
	endif

		move.w	d1,a0					; load free node
		move.w	(a0),(Next_Free_Obj).w			; link free node after this one in as the next one to use
		lea	(Obj_Lists+$02).w,a1			; load start of list heads (plus offset for tail pointer)
		adda.w	d0,a1					; get proper list
		move.w	(a1),a2					; load last object added to this list (if none have been added, it'll be the list head)
		move.w	a0,(a1)					; link new object in as the list tail
		move.w	a2,Obj.Prev(a0)				; link this list's last object into the new one
		move.w	(a2),(a0)				; transfer end of list token to this object
		move.w	a0,(a2)					; link this object in, now setting it as the last object in this list

.Return:
		rts					; 16c	; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to remove the current object from the active list and clear its
; contents, then run the next object
; ---------------------------------------------------------------------------
; INPUTS:
; 	a6.l - Current object
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

RemoveCurrentObject:
		move.w	a6,a0
		move.w	(a6),-(sp)
		bsr.s	RemoveObject

		move.w	(sp)+,d0			; 8c	; get next object pointer
		bpl.s	.NextChain			; 10/8c	; if positive value (end of chain token), branch

		move.w	d0,a6				; 4c	; load as object address
		jmp	Obj.Jmp(a6)			; 10c	; jump to routine for next object

.NextChain:
		beq.s	.End				; 10/8c	; if end of chain token is null, return (end of last chain)

		lea	(Obj_Lists).w,a6		; 8c	; load start of object list heads
		move.w	(a6,d0.w),d0			; 14c	; get start value from next chain
		bpl.s	.NextChain			; 10/8c	; if it's another token, check again

		move.w	d0,a6				; 4c	; load as object address
		jmp	Obj.Jmp(a6)			; 10c	; jump to routine for next object

.End:
		rts					; 16c	; return

; ---------------------------------------------------------------------------
; Function to remove an object from an active list and clear its contents
; ---------------------------------------------------------------------------
; INPUTS:
; 	a0.l - Object to remove
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

RemoveObject:
	if (DEBUG_OBJECTS)
		subq.w	#$01,(Active_Objects).w	; hack
	endif

		move.w	Obj.Prev(a0),a2				; load previous object
		move.w	(a0),d0					; get pointer to next object
		bpl.s	.ListEnd				; if it's a token, branch (this is the list end, so no need to )

		move.w	d0,a1					; load next object
		move.w	a2,Obj.Prev(a1)				; link previous object into next object

.ListEnd:
		move.w	d0,(a2)					; link next object into previous object
		bmi.s	.FreeObject				; if negative value, skip ahead (don't modify tail pointer)
		bne.s	.NotLastList				; if this isn't the last list, branch
		move.w	#MAX_OBJ_LISTS*$04,d0			; otherwise, use value of maximum lists available * 4

.NotLastList:
		lea	(Obj_Lists-$04+$02).w,a1		; load start of list heads (offset forward by two bytes and back by four for tail pointer)
		move.w	a2,(a1,d0.w)				; save previous object as tail

.FreeObject:
		lea	(Next_Free_Obj).w,a1			; load list head for free object nodes
		move.w	(a1),d0					; get first free node in list

		move.w	d0,(a0)					; link next free object link (either a pointer to the next free node, or the end token if no nodes were available)
		move.w	a0,(a1)					; link current node in as next free one

		lea	Obj.Prev(a0),a1				; load current object starting at previous object pointer
		moveq	#$00,d0					; clear d0

	rept (Obj.End-(Obj.Prev))/4
		move.l	d0,(a1)+				; clear four bytes
	endr

	if ((Obj.End-Obj.Prev)&$02)>0
		move.w	d0,(a1)+				; clear last two bytes
	endif

		move.w	#$4EF9,Obj.Jmp(a0)			; set "jmp"
		move.l	#NextObject,Obj.Jmp_Ptr(a0)		; set pointer to "next object" function (if we add this as a new object without setting a pointer, it'll just do nothing)
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to run all loaded object entries
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

ProcessObjects:
		lea	(Obj_Lists).w,a6		; 12c	; load start of object list heads
	;	jmp	NextObject(pc)			; 10c	; run the first loaded object

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to jump from the current object to the next
; ---------------------------------------------------------------------------
; INPUTS:
; 	a6.l - Current object slot
; 
; RETURNS:
; 	a6.l - Next object slot
; ---------------------------------------------------------------------------

_NextObject_NoJmp	macro
		move.w	(a6),d0				; 8c	; get next object pointer
		bpl.s	.NextChain			; 10/8c	; if positive value (end of chain token), branch

		move.w	d0,a6				; 4c	; load as object address
		jmp	Obj.Jmp(a6)			; 10c	; jump to routine for next object

.NextChain:
		beq.s	.End				; 10/8c	; if end of chain token is null, return (end of last chain)

		lea	(Obj_Lists).w,a6		; 8c	; load start of object list heads
		move.w	(a6,d0.w),d0			; 14c	; get start value from next chain
		bpl.s	.NextChain			; 10/8c	; if it's another token, check again

		move.w	d0,a6				; 4c	; load as object address
		jmp	Obj.Jmp(a6)			; 10c	; jump to routine for next object

.End:
		rts					; 16c	; return
	endm

NextObject:
		_NextObject_NoJmp			; perform the code in the macro above

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to set up the sprite layer lists
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

SetupSprites:
		lea	(Sprite_Layer_Lists).w,a0
		moveq	#$04,d0
		rept	SPR_LAYERS-1
			move.w	a0,d1
			move.w	d0,(a0)+
			move.w	d1,(a0)+
			addq.w	#$04,d0
		endr
		clr.w	(a0)+
		move.w	d1,(a0)+
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to add an object to a sprite layer list
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.w - Sprite layer ID (multiples of 4)
; 	a6.l - Object slot
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

AddSprite:
		lea	Obj.Next_Spr+$04(a6),a0			; load sprite links for this object
		lea	(Sprite_Layer_Lists+$02).w,a1		; load sprite lists head (offset by two bytes)
		adda.w	d0,a1					; advance to proper layer list
		move.w	(a1),a2					; load last object in list
		move.w	a2,-(a0)				; set prev pointer to last sprite
		move.w	(a2),-(a0)				; set next pointer to end token
		move.w	a0,(a2)					; save new sprite in last sprite's next pointer
		move.w	a0,(a1)					; save new sprite as last sprite in list
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to remove an object from a sprite layer list
; ---------------------------------------------------------------------------
; INPUTS:
; 	a6.l - Object slot
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

RemoveSprite:
	; FAILSAFE: ensure object isn't removed from a list if it was
	; never added (doing so will break the handler)

		tst.l	Obj.Next_Spr(a6)			; has object been added as a sprite?
		beq.s	.Return					; if not, branch

		lea	Obj.Prev_Spr(a6),a0			; load sprite links for this object
		move.w	(a0),a1					; load prev pointer
		move.w	-(a0),d0				; load next pointer
		bpl.s	.LastSprite				; if positive, branch (token for end of list)
		move.w	d0,a2					; load next pointer
		move.w	a1,2(a2)				; set prev pointer in next sprite
		move.w	a2,(a1)					; set next pointer in prev sprite
		moveq	#0,d0					; clear d0
		move.l	d0,(a0)					; clear sprite links
		rts						; return

.LastSprite:
		move.w	d0,(a1)					; set token in prev sprite
		lea	(Sprite_Layer_Lists+$02).w,a2		; get tail pointer in list head
		adda.w	Obj.Spr_Layer(a6),a2			; ''
		move.w	a1,(a2)					; set prev as tail pointer in list head
		moveq	#0,d0					; clear d0
		move.l	d0,(a0)					; clear sprite links

.Return:
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to render all objects added to sprite layer lists
; ---------------------------------------------------------------------------
; INPUTS:
; 	none
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

RenderObjects:
		lea	(Sprite_Layer_Lists).w,a6		; load sprite layer list heads
		lea	(Sprite_Buffer).w,a1			; load sprite buffer
		moveq	#$00,d0					; clear d0 (will be used as sprite counter)
		bra.s	.Loop					; begin loop

.NextChain:
		beq.s	.Return					; if token is null value, branch (no more objects to render)
		lea	(Sprite_Layer_Lists).w,a6		; reload sprite layer list heads
		adda.w	d2,a6					; add offset for next list

.Loop:
		move.w	(a6),d2					; get pointer to next node
		bpl.s	.NextChain				; if positive, branch (end of list token)
		move.w	d2,a6					; load object
		tst.b	Obj.Hide-Obj.Next_Spr(a6)		; is object set to be hidden this frame?
		bne.s	.Loop					; if so, skip this object
		move.l	Obj.Maps-Obj.Next_Spr(a6),a0		; load mappings address
		move.w	Obj.Frame-Obj.Next_Spr(a6),d2		; load sprite frame (multiples of 2)
		move.w	(a0,d2.w),a2				; get word-relative offset of frame data from index start
		adda.l	a2,a0					; add offset to get proper location
		move.w	(a0)+,d7				; get piece count for this frame
		move.w	Obj.X_Pos-Obj.Next_Spr(a6),d1		; get X position
		move.w	Obj.Y_Pos-Obj.Next_Spr(a6),d2		; get Y position
		addi.w	#128,d1					; offset properly
		addi.w	#128,d2					; ''
		move.w	Obj.Render_Flags-Obj.Next_Spr(a6),d3	; get VDP render flags
		move.w	Obj.Tile-Obj.Next_Spr(a6),a2		; get base tile ID
		moveq	#%00011000,d4				; get X/Y flip flags
		and.b	Obj.Render_Flags-Obj.Next_Spr(a6),d4	; ''
		bsr.s	DrawSpriteMappings			; draw mappings
		bne.s	.Loop					; if we haven't reached the sprite limit, restart loop

.Return:
		tst.b	d0					; check number of sprites drawn
		beq.s	.NoSprites				; if none were drawn, branch
		clr.b	-$05(a1)				; clear next link of last sprite
		rts						; return

.NoSprites:
		move.l	d0,(a1)+				; ensure first sprite entry is cleared
		move.l	d0,(a1)+				; ''
		rts						; return

; ---------------------------------------------------------------------------
; Function to draw a set of sprite pieces
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.w - Number of sprite pieces currently drawn
; 	d1.w - X position
; 	d2.w - Y position
; 	d3.w - VDP flags
; 	d4.b - VDP flags (X/Y flip only) (MUST be written immediately before calling function)
; 	d7.w - Number of pieces to draw for this frame
; 	a0.l - Location of mappings
; 	a1.l - Sprite buffer
; 	a2.w - Tile ID (as data, not pointer)
; 	a6.l - Object node (offset by sprite link) (not actually used here, but don't overwrite)
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------
; NOTES: 
; 	Sprite piece input format is as follows:
;		dc.b	00
;		dc.b	$SZ
;		dc.w	$YPOS
;		dc.w	$XPOS
;		dc.w	$TILE
; ---------------------------------------------------------------------------

DrawSpriteMappings:
		bne.s	.Flipped				; if object is X/Y flipped, branch

	; For objects not flipped

.NoFlip:
		addq.w	#$01,d0					; increment sprite count
		move.l	(a0)+,d5				; get first long-word
		add.w	d2,d5					; add Y position
		swap	d5					; swap words
		move.b	d5,-(sp)				; push size byte (no need to use it for size lookup here)
		move.w	(sp)+,d5				; pop size byte to upper byte
		move.b	d0,d5					; set next link
		move.l	d5,(a1)+				; save first long-word

		move.l	(a0)+,d5				; get second long-word
		add.w	a2,d5					; add base tile ID to tile offset
		or.w	d3,d5					; apply render flags
		swap	d5					; swap words
		add.w	d1,d5					; add X position
		move.l	d5,(a1)+				; save second word
		cmpi.b	#80,d0					; have we drawn the maximum number of sprites?
		dbeq	d7,.NoFlip				; don't draw the next piece if we have, or loop again if we haven't and there's another to draw
		rts						; return

; ---------------------------------------------------------------------------

.Flipped:
		lea	.HorizSizes(pc),a3			; load table of horizontal sizes
		lea	$10(a3),a4				; load table of vertical sizes
		move.b	d4,-(sp)				; push byte (containing X/Y flip flags)
		jmp	.Flips(pc,d4.w)				; jump properly

.HorizSizes:	dc.b	8, 8, 8, 8
		dc.b	16, 16, 16, 16
		dc.b	24, 24, 24, 24
		dc.b	32, 32, 32, 32

.VertiSizes:	dc.b	8, 16, 24, 32
		dc.b	8, 16, 24, 32

.Flips:
	; $00, No flip (won't be used, so let's fit the last eight bytes of the vertical size table here)

		dc.b	8, 16, 24, 32
		dc.b	8, 16, 24, 32

	; $08, X-flip

		move.w	(sp)+,d4				; pop to upper byte
		clr.b	d4					; clear lower byte
		eor.w	d4,d3					; remove X flag from render flags
		bra.s	.XFlip					; start loop

	; $10, Y-flip

		move.w	(sp)+,d4				; pop to upper byte
		clr.b	d4					; clear lower byte
		eor.w	d4,d3					; remove Y flag from render flags
		bra.s	.YFlip					; start loop

	; $18, X/Y-flip

		move.w	(sp)+,d4				; pop to upper byte
		clr.b	d4					; clear lower byte
		eor.w	d4,d3					; remove X and Y flags from render flags

; ---------------------------------------------------------------------------
; For horizontally and vertically flipped objects
; ---------------------------------------------------------------------------

.XYFlip:
		addq.w	#$01,d0					; increment sprite count
		move.l	(a0)+,d5				; get first long-word
		neg.w	d5					; invert Y offset
		add.w	d2,d5					; add Y position
		swap	d5					; swap words
		moveq	#$00,d6					; clear d4
		move.b	(a4,d5.w),d6				; get vertical size
		swap	d6					; swap words
		sub.l	d6,d5					; add vertical size to Y offset
		move.b	(a3,d5.w),d6				; get horizontal size (for later)
		move.b	d5,-(sp)				; push size byte
		move.w	(sp)+,d5				; pop size byte to upper byte
		move.b	d0,d5					; set next link
		move.l	d5,(a1)+				; save first long-word

		move.l	(a0)+,d5				; get second long-word
		add.w	a2,d5					; add base tile ID to tile offset
		or.w	d3,d5					; apply render flags
		eor.w	d4,d5					; flip X- and Y-flip flags
		swap	d5					; swap words
		neg.w	d5					; invert X offset
		sub.w	d6,d5					; add horizontal size
		add.w	d1,d5					; add X position
		move.l	d5,(a1)+				; save second long-word
		cmpi.b	#80,d0					; have we drawn the maximum number of sprites?
		dbeq	d7,.XYFlip				; don't draw the next piece if we have, or loop again if we haven't and there's another to draw
		rts						; return

; ---------------------------------------------------------------------------
; For horizontally flipped objects
; ---------------------------------------------------------------------------

.XFlip:
		addq.w	#$01,d0					; increment sprite count
		move.l	(a0)+,d5				; get first long-word
		add.w	d2,d5					; add Y position
		swap	d5					; swap words
		moveq	#$00,d6					; clear d4
		move.b	(a3,d5.w),d6				; get horizontal size
		move.b	d5,-(sp)				; push size byte
		move.w	(sp)+,d5				; pop size byte to upper byte
		move.b	d0,d5					; set next link
		move.l	d5,(a1)+				; save first long-word

		move.l	(a0)+,d5				; get second long-word
		add.w	a2,d5					; add base tile ID to tile offset
		or.w	d3,d5					; apply render flags
		eor.w	d4,d5					; flip X-flip flag
		swap	d5					; swap words
		neg.w	d5					; invert X offset
		sub.w	d6,d5					; add X size
		add.w	d1,d5					; add X position
		move.l	d5,(a1)+				; save second long-word
		cmpi.b	#80,d0					; have we drawn the maximum number of sprites?
		dbeq	d7,.XFlip				; don't draw the next piece if we have, or loop again if we haven't and there's another to draw
		rts						; return

; ---------------------------------------------------------------------------
; For vertically flipped objects
; ---------------------------------------------------------------------------

.YFlip:		
		addq.w	#$01,d0					; increment sprite count
		move.l	(a0)+,d5				; get first long-word
		neg.w	d5					; invert Y offset
		add.w	d2,d5					; add Y position
		swap	d5					; swap words
		moveq	#$00,d6					; clear d4
		move.b	(a4,d5.w),d6				; get vertical size
		swap	d6					; swap words
		sub.l	d6,d5					; add size to offset
		move.b	d5,-(sp)				; push size byte
		move.w	(sp)+,d5				; pop size byte to upper byte
		move.b	d0,d5					; set next link
		move.l	d5,(a1)+				; save first long-word

		move.l	(a0)+,d5				; get second long-word
		add.w	a2,d5					; add base tile ID to tile offset
		or.w	d3,d5					; apply render flags
		eor.w	d4,d5					; flip Y-flip flag
		swap	d5					; swap words
		add.w	d1,d5					; add X position
		move.l	d5,(a1)+				; save second long-word
		cmpi.b	#80,d0					; have we drawn the maximum number of sprites?
		dbeq	d7,.YFlip				; don't draw the next piece if we have, or loop again if we haven't and there's another to draw
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Update object position using velocity
; ---------------------------------------------------------------------------
; INPUTS:
; 	a6.l - object RAM slot
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------
; NOTES:
; 
; velocity is stored in the balls
; 
; 	$pixels.subpixels
; 
; ---------------------------------------------------------------------------

ObjVelToPos:
		movem.w	Obj.X_Vel(a6),d0-d1			; get object's X and Y velocity
		ext.l	d0					; sign-extend X
		ext.l	d1					; sign-extend Y
		lsl.l	#8,d0					; multiply X by $100
		lsl.l	#8,d1					; multiply Y by $100
		add.l	d0,Obj.X_Pos(a6)			; update X position
		add.l	d1,Obj.Y_Pos(a6)			; update Y position
		rts						; return

; ===========================================================================
; ---------------------------------------------------------------------------
; Function to apply friction to an object's velocity
; ---------------------------------------------------------------------------
; INPUTS:
; 	d0.w - Friction value ($pixels.subpixels)
; 	a0.l - Storage location of velocity (word value, $pixels.subpixels)
; 
; RETURNS:
; 	none
; ---------------------------------------------------------------------------

ObjApplyFriction:
		move.w	(a0),d1					; get current velocity
		beq.s	.Return					; if it's zero, return (we don't need to do anything)
		bpl.s	.Positive				; if it's a positive value, branch

		add.w	d0,d1					; add friction value
		bpl.s	.ClearVel				; branch if sign wraps
		move.w	d1,(a0)					; save velocity
		rts						; return

.Positive:
		sub.w	d0,d1					; subtract friction value
		bmi.s	.ClearVel				; branch if sign wraps
		move.w	d1,(a0)					; save velocity
		rts						; return

.ClearVel:
		clr.w	(a0)					; clear velocity

.Return:
		rts						; return