; ===========================================================================
; ---------------------------------------------------------------------------
; Twiz.var.asm - RAM variables for Twizzler library
; ---------------------------------------------------------------------------
; ---------------------------------------------------------------------------
; RAM structure
; ---------------------------------------------------------------------------

Twiz_Buffer_Pre		rs.b	TWIZ_BUFFER_SIZE
Twiz_Buffer		rs.b	TWIZ_BUFFER_SIZE

Twiz_Huff_Ret		rs.b	TWIZ_HUFF_RET_MAX*$04
Twiz_Huff_Copy		rs.b	TWIZ_HUFF_COPY_MAX*$02
Twiz_VRAM		rs.b	$04
Twiz_Size		rs.b	$02